import axios from "axios";
const BASE_URL = "https://dummyapi.io/data/v1/";
const APP_ID = "61c01b4eaec39b388ea1b47b";

const createUser = async (data) => {
  let newUser = await axios.post(BASE_URL + "user/create", data, {
    headers: {
      "Content-Type": "multipart/form-data",
      "app-id": APP_ID,
    },
  });
  return newUser;
};

const loginUser = async (data) => {
  let login = await axios.get(BASE_URL + "user", data, {
    headers: {
      "Content-Type": "multipart/form-data",
      "app-id": APP_ID,
    },
  });
  return login;
};

export { createUser, loginUser };
