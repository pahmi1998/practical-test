import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "assets/plugins/nucleo/css/nucleo.css";
import "assets/scss/argon-dashboard-react.scss";
import "@fortawesome/fontawesome-free/css/all.min.css";

import Login from "pages/Login";
import Registration from "pages/Registration";
import Home from "pages/Home";

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Login} />
        <Route path="/registration" component={Registration} />
        <Route path="/home" component={Home} />
      </Router>
    </div>
  );
}

export default App;
