import React from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
  Container,
} from "reactstrap";

function Login() {
  const history = useHistory();
  return (
    <div className="main-content">
      <div
        className="header bg-gradient-info py-10 py-lg-9"
        style={{ height: "960px" }}
      >
        <Container>
          <div className="header-body text-center mb-4">
            <Row className="justify-content-center">
              <Col lg="5" md="6">
                <h1 className="text-white">Berhasil Masuk!</h1>
                <h2>
                  <a
                    className="text-light"
                    href="#pablo"
                    onClick={() => history.push("/login")}
                  >
                    <small>Logout</small>
                  </a>
                </h2>
              </Col>
            </Row>
          </div>
        </Container>
        {/* Page content */}
        {/* <Container className="pb-5">
          <Row className="justify-content-center">
            <Col lg="5" md="7">
              <Card className="bg-secondary shadow border-0">
                <CardBody className="px-lg-5 py-lg-5">
                  <div className="text-center text-muted mb-4">
                    <small>Silahkan Masuk</small>
                  </div>
                  <Form role="form">
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-email-83" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Email"
                          type="email"
                          autoComplete="new-email"
                          required
                        />
                      </InputGroup>
                    </FormGroup>
                    <div className="text-center">
                      <Button className="my-4" color="primary" type="button">
                        Masuk
                      </Button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
              <Row className="mt-3">
                <Col className="text-center" md="12">
                  <h2>
                    <a
                      className="text-light"
                      href="#pablo"
                      onClick={() => history.push("/registration")}
                    >
                      <small>Buat Akun</small>
                    </a>
                  </h2>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container> */}
      </div>
    </div>
  );
}

export default Login;
