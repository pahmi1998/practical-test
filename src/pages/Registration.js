import { createUser } from "api/Users";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroup,
  Row,
  Col,
  Container,
} from "reactstrap";

function Registration() {
  const [value, setValue] = useState(null);
  const history = useHistory();

  const handleChangeInput = (event) => {
    let name = event.target.name;
    setValue({ ...value, [name]: event.target.value });
  };

  const handleFinish = () => {
    console.log(value);
    let formData = new FormData();
    formData.append("firstName", value?.firstName);
    formData.append("lastName", value?.lastName);
    formData.append("email", value?.email);
    createUser(formData).then((data) => history.push("/"));
  };

  return (
    <div className="main-content">
      <div
        className="header bg-gradient-info py-10 py-lg-9"
        style={{ height: "960px" }}
      >
        <Container>
          <div className="header-body text-center mb-4">
            <Row className="justify-content-center">
              <Col lg="5" md="6">
                <h1 className="text-white">Selamat Datang!</h1>
              </Col>
            </Row>
          </div>
        </Container>
        {/* Page content */}
        <Container className="pb-5">
          <Row className="justify-content-center">
            <Col lg="5" md="7">
              <Card className="bg-secondary shadow border-0">
                <CardBody className="px-lg-5 py-lg-5">
                  <div className="text-center text-muted mb-4">
                    <small>Lakukan Pendaftaran Disini</small>
                  </div>
                  <Form>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="First Name"
                          type="text"
                          autoComplete="firstName"
                          name="firstName"
                          onChange={handleChangeInput}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="Last Name"
                          type="text"
                          autoComplete="lastName"
                          name="lastName"
                          onChange={handleChangeInput}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Input
                        placeholder="Email"
                        type="email"
                        autoComplete="new-email"
                        name="email"
                        onChange={handleChangeInput}
                        required
                      />
                    </FormGroup>
                    <div className="text-center">
                      <Button
                        className="my-4"
                        color="primary"
                        type="button"
                        onClick={handleFinish}
                      >
                        Daftar
                      </Button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
              <Row className="mt-3">
                <Col className="text-center" md="12">
                  <h2>
                    <a
                      className="text-light"
                      href="#pablo"
                      onClick={() => history.push("/login")}
                    >
                      <small>Login</small>
                    </a>
                  </h2>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Registration;
