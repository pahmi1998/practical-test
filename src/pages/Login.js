import { loginUser } from "api/Users";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
  Container,
} from "reactstrap";

function Login() {
  const [value, setValue] = useState(null);
  const history = useHistory();

  const handleChangeInput = (event) => {
    let name = event.target.name;
    setValue({ ...value, [name]: event.target.value });
  };

  const handleFinish = () => {
    console.log(value);
    let formData = new FormData();
    formData.append("email", value?.email);
    loginUser(formData).then((data) => history.push("/Home"));
  };

  return (
    <div className="main-content">
      <div
        className="header bg-gradient-info py-10 py-lg-9"
        style={{ height: "960px" }}
      >
        <Container>
          <div className="header-body text-center mb-4">
            <Row className="justify-content-center">
              <Col lg="5" md="6">
                <h1 className="text-white">Selamat Datang!</h1>
              </Col>
            </Row>
          </div>
        </Container>
        {/* Page content */}
        <Container className="pb-5">
          <Row className="justify-content-center">
            <Col lg="5" md="7">
              <Card className="bg-secondary shadow border-0">
                <CardBody className="px-lg-5 py-lg-5">
                  <div className="text-center text-muted mb-4">
                    <small>Silahkan Masuk</small>
                  </div>
                  <Form role="form">
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-email-83" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Email"
                          type="email"
                          autoComplete="new-email"
                          required
                          onChange={handleChangeInput}
                        />
                      </InputGroup>
                    </FormGroup>
                    <div className="text-center">
                      <Button
                        className="my-4"
                        color="primary"
                        type="button"
                        onClick={handleFinish}
                      >
                        Masuk
                      </Button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
              <Row className="mt-3">
                <Col className="text-center" md="12">
                  <h2>
                    <a
                      className="text-light"
                      href="#pablo"
                      onClick={() => history.push("/registration")}
                    >
                      <small>Buat Akun</small>
                    </a>
                  </h2>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Login;
